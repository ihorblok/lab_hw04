use I_B_module_3
go
-- create database

CREATE DATABASE [I_B_module_3]
go


-- create tables laptop and product



CREATE TABLE [laptop] (

		id_laptop        	    INT NOT NULL ,
		serial 			        int NOT NULL ,
		model             	    TEXT NOT NULL ,
		speed         			TEXT NOT NULL ,
		ram          			TEXT NULL ,
		hd         				TEXT NULL ,
		dvd_rom			        TEXT NULL ,
		price  			        NUMERIC(18,2) NOT NULL ,
		screen 			        INT NULL  check (screen >= 0),
		date_making   			DATETIME NOT NULL ,
		inserted_date			DATE NOT NULL ,
		updated_date  			DATE NOT NULL ,

 CONSTRAINT PK_PC PRIMARY KEY CLUSTERED ([id_laptop]),
 CONSTRAINT uk_laptop unique ([serial])
 )
GO

CREATE TABLE [product] (

		id_product   			 INT NOT NULL ,
		type         			 TEXT NOT NULL ,
		maker        			 TEXT NOT NULL ,
		supplier     			 TEXT NOT NULL ,
		invoice     			 NUMERIC(18,0) NOT NULL  default (0),
		id_laptop     			 INT NOT NULL ,
		date          			 DATE NOT NULL ,
		city          			 TEXT NULL ,
		address       			 TEXT NULL ,
		postal_code       	     TEXT NULL ,
		inserted_date			 DATE NOT NULL ,
		updated_date      		 DATE NOT NULL ,

 CONSTRAINT PK_product PRIMARY KEY CLUSTERED ([id_product]),
  
)
GO


-- add foreign key

ALTER TABLE I_B_module_3.product
ADD CONSTRAINT [FK_LAPTOP] FOREIGN KEY ([id_laptop])
REFERENCES I_B_module_3.laptop ([id_laptop])

-- INSERT product

INSERT INTO I_B_module_3.laptop
		([id_laptop]
		,[serial]
		,[model]
		,[speed]
		,[ram]
		,[hd]
		,[dvd_rom]
		,[price] 
		,[screen]
		,[date_making]
		,[inserted_date]
		,[updated_date]  	)	  
VALUES
           ( 1
		    ,445566
			,'HP4551'
			,'3.6 GHz'
			,'8G'
			,'256G'
			,NULL
			,1526.56
			,24
			,'01-01-2016'
			,'02-02-2017'
			,'03-03-2018'
		    )
GO

-- INSERT laptop

INSERT INTO I_B_module_3.product
			([id_product]
			,[type]         			
			,[maker]        			
			,[supplier]     			 
			,[invoice]     				 
			,[date] 
			,id_laptop        			 
			,[city]          			 
			,[address]       			 
			,[postal_code]       	     
			,[inserted_date]			 
			,[updated_date])   		  
VALUES
           (1
			,'ultrabook'
			,'HP'
			,'Braine'
			,1568456
			,'08-06-2018'
			,1
			,'Lviv'
			,'S.Bandery, 27'
			,'79056'
			,'01-03-2017'
			,'01-02-2018'
		    )
GO

select * from I_B_module_3.laptop

UPDATE I_B_module_3.product
SET [updated_date] = '01-03-2000', [city]= 'Frankfurt'
WHERE [id_product] = 1

UPDATE I_B_module_3.laptop
SET [screen] = -21
WHERE [id_laptop] = 1
